import React from 'react'
import { Spin } from 'antd';
import 'antd/dist/antd.css'; 
export default () => {
    return (
        <div className="example" 
        style={{
            textAlign: 'center'
        }}>
            <Spin />
        </div>
    
    )
}
